<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentarioUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentario_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('comentario_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->decimal('salario', 8, 2)->nullable();
            $table->boolean('like')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentario_user');
    }
}
