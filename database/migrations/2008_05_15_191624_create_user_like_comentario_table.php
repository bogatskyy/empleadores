<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLikeComentarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_like_comentario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('comentario_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('like')->default('0');
            $table->boolean('dislike')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_like_comentario');
    }
}
