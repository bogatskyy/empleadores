<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('propietario')->default(0);
            $table->smallInteger('valoracion')->nullable();
            $table->smallInteger('equilibrio')->nullable();
            $table->smallInteger('beneficios')->nullable();
            $table->smallInteger('estabilidad')->nullable();
            $table->smallInteger('gestion')->nullable();
            $table->smallInteger('cultura')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_user');
    }
}
