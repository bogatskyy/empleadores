<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('anuncio_id');
            $table->string('cif', 191)->unique()->nullable();
            $table->string('empresa', 191)->unique();
            $table->boolean('sucursal');
            $table->string('registro', 255)->nullable();
            $table->string('datos_registrales', 255);
            $table->boolean('liquidacion');
            $table->bigInteger('total_comentarios')->nullable();
            $table->bigInteger('total_likes')->nullable();
            $table->bigInteger('total_valoracion')->nullable();
            $table->string('foto', 255)->default('default_company_foto.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
