<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EmpresasController@index')->name('home');

Route::get('/home', 'EmpresasController@index')->name('home');

Auth::routes();

Route::get('/empresas', 'EmpresasController@index')->name('home');

//rutas de empresas:
Route::resource('empresas', 'EmpresasController');

//ruta para buscador autocomplete:
Route::get('/search', 'EmpresasController@search')->name('empresas.search');
//ruta para buscador BUSCAR(submit):
Route::get('empresas/provincias/{provincia?}/{provinciaId?}/{letras?}', 'EmpresasController@provincias')->name('empresas.provincias.index');

//rutas para los comentarios:
Route::post('/comentarios', 'RequestController@comentarios');
Route::post('/guardarComentarios', 'RequestController@guardarComentarios');

//rutas para las respuestas:
Route::post('/guardarRespuestas', 'RequestController@guardarRespuestas');

//rutas para los likes de comentarios:
Route::post('/guardarLikeComentario', 'RequestController@guardarLikeComentario');
Route::post('/guardarDislikeComentario', 'RequestController@guardarDislikeComentario');

//rutas para actos:
Route::post('/actos', 'RequestController@actos');

//rutas para los bormes:
Route::post('/bormes', 'RequestController@bormes');
//rutas para el socialite:
Route::get('redirect/{driver}', 'Auth\LoginController@redirectToProvider')
    ->name('login.provider')
    ->where('driver', implode('|', config('auth.socialite.drivers')));

Route::get('{driver}/callback', 'Auth\LoginController@handleProviderCallback')
    ->name('login.callback')
    ->where('driver', implode('|', config('auth.socialite.drivers')));

//rutas para los empleadores:
Route::get('/oferta/{empresa?}', function ($empresa = "") {
    return view('empleadores.index', compact('empresa'));
});
Route::get('/formulario/{empresa?}', function ($empresa = "") {
    return view('empleadores.formulario',compact('empresa'));
});

Route::get('/valorar/{empresa?}', function ($empresa = "") {
    return view('empresas.valorar',compact('empresa'));
});

Route::post('/valorar/guardarValoracion', 'ValoracionesController@store');