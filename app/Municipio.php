<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    public function localidads()
    {
        return $this->hasMany('App\Localidad');
    }

    public function provincia()
    {
        return $this->belongsTo('App\Provincia');
    }
}
