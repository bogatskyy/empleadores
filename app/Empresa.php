<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;

class Empresa extends Model
{
    use Rateable;
    public function bormes()
    {
        return $this->belongsToMany('App\Borme');
    }

    public function actos()
    {
        return $this->hasMany('App\Acto');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('propietario', 'valoracion', 'equilibrio', 'beneficios', 'estabilidad', 'gestion', 'cultura');
    }

    public function comentarios()
    {
        return $this->hasMany('App\Comentario');
    }

    public function provincias()
    {
        return $this->belongsToMany('App\Provincia');
    }

    public function getRouteKeyName()
    {
        return 'empresa';
    }
}
