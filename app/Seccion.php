<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    public function bormes()
    {
        return $this->belongsToMany('App\Borme');
    }
}
