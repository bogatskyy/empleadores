<?php

namespace App\Http\Controllers;

use http\Client\Curl\User;
use Illuminate\Http\Request;

class ValoracionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('empresas.index', compact('empresas', 'provincias'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Añadimos la valoracion
        //importamos los datos de la empresa
        $empresa = \App\Empresa::where('empresa' , '=', $request->nombre)->get()->first();

        foreach ( $empresa->users as $user){
            if( $user->pivot->user_id == $request->user()->id){
                return redirect(url('/')."/empresas/".$request->nombre);
             }
        }



        $empresa->users()->attach($request->user()->id, ['cultura'=> $request->es5 , 'gestion'=> $request->es4 ,'estabilidad'=> $request->es3 ,'beneficios'=> $request->es2 ,'equilibrio'=> $request->es1 , 'valoracion'=> $request->es0 , 'empresa_id'=> $empresa->id]);
        //Refrescar media

        $empresa = \App\Empresa::where('empresa' , '=', $request->nombre)->get()->first();
        $count = 0;
        $sum = 0;
        foreach ( $empresa->users as $user){
            $sum += $user->pivot->valoracion;
            $count++;
        }
        $media =  $sum/$count;
        $empresa->total_valoracion = $media;
        $empresa->save();
        return redirect(url('/')."/empresas/".$request->nombre);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($empresa)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($empresa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {

    }


}
