<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    //
    public function actos(Request $request) {

        $actos = \App\Empresa::find($request->id)->actos()->get();

        return $actos;
    }

    public function bormes(Request $request) {

        $bormes = \App\Empresa::find($request->id)->bormes()->get();

        return $bormes;
    }

    public function insertRespuestas ($comentarios) {
        foreach ($comentarios as $comentario) {
            $user = $comentario->users()->first();
            $comentario->avatar = $user->avatar;
            $comentario->name = $user->name;
            $respuestas = $comentario->respuestas()->get();
            foreach ($respuestas as $respuesta) {
                $respuesta->user = $respuesta->users()->first();
            }
            $comentario->respuestas = $respuestas;
        }
        return $comentarios;
    }

    public function comentarios(Request $request) {

        $comentarios = \App\Empresa::find($request->id)->comentarios()->get();

        $comentarios = $this->insertRespuestas($comentarios);

        return $comentarios;
    }

    public function guardarComentarios (Request $request) {
        $comentario = new \App\Comentario();
        $comentario->titulo = $request->titulo;
        $comentario->descripcion = $request->descripcion;
        $comentario->empresa_id = $request->empresa_id;
        $comentario->save();

        $empresa = \App\Empresa::find($request->empresa_id);
        $empresa->total_comentarios = $empresa->total_comentarios + 1;
        $empresa->save();

        //modificacion tabla intermedia
        $comentario->users()->attach($request->user_id);

        $comentarios = \App\Empresa::find($request->empresa_id)->comentarios()->get();
        $comentarios = $this->insertRespuestas($comentarios);
        return $comentarios;
    }

    public function guardarRespuestas (Request $request) {
        $respuesta = new \App\Respuesta();
        $respuesta->titulo = $request->titulo;
        $respuesta->descripcion = $request->descripcion;
        $respuesta->comentario_id = $request->comentario_id;
        $respuesta->save();

        //modificacion tabla intermedia
        $respuesta->users()->attach($request->user_id);

        $comentarios = \App\Empresa::find($request->empresa_id)->comentarios()->get();
        $comentarios = $this->insertRespuestas($comentarios);
        return $comentarios;
    }

    public function guardarLikeComentario (Request $request) {
        //encontramos el comentario necesario:
        $comentario = \App\Comentario::find($request->comentario_id);
        //sacamos el objeto de like entre el User - Comentario
        $like = DB::table('user_like_comentario')
            ->where('user_id', '=', $request->user_id)
            ->where('comentario_id', '=', $request->comentario_id)
            ->first();

        //si existe el like en tabla intermedia:
        if (isset($like)) {
            if ($like->like == 0 && $like->dislike == 0) {
                //modificamos like de tabla intermedia a TRUE
                $comentario->usersLikes()->sync([$request->user_id => ['like'=> 1]]);
                //subimos los likes totales del comentario:
                $comentario->total_likes = $comentario->total_likes + 1;
                $comentario->save();
            }
            else if ($like->like == 1) {
                //modificamos like de tabla intermedia a FALSE
                $comentario->usersLikes()->sync([$request->user_id => ['like'=> 0]]);
                //bajamos los likes totales del comentario:
                $comentario->total_likes = $comentario->total_likes - 1;
                $comentario->save();
            }
        }
        //si no existe el like en la tabla intermedia:
        else {
            $comentario->usersLikes()->attach([$request->user_id => ['like'=> 1]]);
            $comentario->total_likes = $comentario->total_likes + 1;
            $comentario->save();
        }
        $comentarios = \App\Empresa::find($request->empresa_id)->comentarios()->get();
        $comentarios = $this->insertRespuestas($comentarios);
        return $comentarios;
    }
    public function guardarDislikeComentario (Request $request) {
        //encontramos el comentario necesario:
        $comentario = \App\Comentario::find($request->comentario_id);
        //sacamos el objeto de dislike entre el User - Comentario
        $dislike = DB::table('user_like_comentario')
            ->where('user_id', '=', $request->user_id)
            ->where('comentario_id', '=', $request->comentario_id)
            ->first();

        //si existe el dislike en tabla intermedia:
        if (isset($dislike)) {
            if ($dislike->dislike == 0 && $dislike->like == 0) {
                //modificamos dislike de tabla intermedia a TRUE
                $comentario->usersLikes()->sync([$request->user_id => ['dislike'=> 1]]);
                //subimos los likes totales del comentario:
                $comentario->total_dislikes = $comentario->distotal_likes + 1;
                $comentario->save();
            }
            else if ($dislike->dislike == 1) {
                //modificamos dislike de tabla intermedia a FALSE
                $comentario->usersLikes()->sync([$request->user_id => ['dislike'=> 0]]);
                //bajamos los likes totales del comentario:
                $comentario->total_dislikes = $comentario->total_dislikes - 1;
                $comentario->save();
            }
        }
        //si no existe el dislike en la tabla intermedia:
        else {
            $comentario->usersLikes()->attach([$request->user_id => ['dislike'=> 1]]);
            $comentario->total_dislikes = $comentario->total_dislikes + 1;
            $comentario->save();
        }
        $comentarios = \App\Empresa::find($request->empresa_id)->comentarios()->get();
        $comentarios = $this->insertRespuestas($comentarios);
        return $comentarios;
    }
}
