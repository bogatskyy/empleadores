<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\File;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = \App\Empresa::has('comentarios')
            ->simplePaginate(9);
        $provincias = \App\Provincia::all();
        return view('empresas.index', compact('empresas', 'provincias'));
    }

    /**
     * muestra empresas por provincias con o sin texto o provincia
     *
     * @param string $provincia
     * @param int $provinciaId
     * @param string $letras
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function provincias ($provincia = "", $provinciaId = 0, $letras = "") {

        //provincias para el buscador:
        $provincias = \App\Provincia::all();

        //en caso de que el usuario ha elegido la provincia:
        if ($provinciaId != 0) {
            $provincia = \App\Provincia::find($provinciaId);

            //en caso de que usuario ha enviado letras desde el buscador:
            if (strlen($letras) != 0) {
                //elegimos todas las empresas que contienen
                //combinación de estas letras de esta provincia:
                $empresas = $provincia->empresas()
                    ->where('empresa', 'LIKE', '%'.$letras.'%')
                    ->simplePaginate(9);
                return view('empresas.provincias.index',
                    compact('provincia', 'letras', 'empresas', 'provincias'));
             }
             //en caso de que el usuario NO ha eligido las letras:
            else {
                $empresas = $provincia->empresas()->simplePaginate(9);
                return view('empresas.provincias.index',
                    compact('provincia', 'letras', 'empresas', 'provincias'));
            }
        }
        else if ($provinciaId == 0) {
            $provincia = new \App\Provincia();
            $provincia->provincia = "todas las provincias";
            //elegimos todas las empresas que contienen
            //combinación de estas letras:
            $empresas = \App\Empresa::
                where('empresa', 'LIKE', '%'.$letras.'%')
                ->simplePaginate(9);
            return view('empresas.provincias.index',
                compact('provincia', 'letras', 'empresas', 'provincias'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($empresa)
    {
        $empresa = \App\Empresa::where('empresa', '=', $empresa)->first();
        try {
            $provincia = $empresa->provincias()->first();
        }
        catch (\Exception $e) {
            $provincia = new \App\Provincia();
            $provincia->provincia = "error en el link";
        }
        return view('empresas.show', compact('empresa', 'provincia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //autorizamos acceso SOLO a los PROPIETARIOS
        $request->user()->authorizeRoles(['propietario']);

        $empresa = \App\Empresa::find($id);

        //comprobamos si es propietario de dicha empresa
        if ($request->user()->propietarioEmpresas()->id == $id) {
            return view('empresas.edit',
                compact('empresa'));
        }

        else {
            abort(401, 'Usted no es el propietario de la empresa');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //autorizamos acceso SOLO a los PROPIETARIOS
        $request->user()->authorizeRoles(['propietario']);

        $empresa =  \App\Empresa::find($id);

        //Imagen
        $ruta = public_path().'/images/';
        if ($request->file('foto') !== null) {

            $imagenNombre = $empresa -> foto;
            //File::delete( $ruta . $imagenNombre);

            // recogida del form
            $imagenOriginal = $request->file('foto');

            // crear instancia de imagen
            $imagen = Image::make($imagenOriginal);

            // generar un nombre aleatorio para la imagen
            $temp_name = $this->random_string() . '.' . $imagenOriginal->getClientOriginalExtension();

            // guardar imagen
            // save( [ruta], [calidad])
            $imagen->save($ruta . $temp_name, 40);

            //Declaro la variable $size la cual contiene el tamaño de la imagen ya guardada, lo divido por 1024 dos veces para que sea en MB.
            $size = $imagen->filesize() / 1024 / 1024;

            //Si el tamaño es mas grande de 0.6 MB.
            if($size > 0.6){
                //Se elimina la imagen.
                File::delete($ruta.$temp_name);

                //Redirijimos a la vista un mensaje de error.
                return redirect()->back()->with('alert', 'Su imagen pesa mas de lo permitido !');
            }

        }
        else {
            $temp_name = $empresa -> foto;
        }
        //Guardo la imagen.
        $empresa->foto = $temp_name;

        $empresa -> save();

        return redirect()->action('EmpresasController@show',$empresa->empresa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        if (isset($request->search)) {
            if ($request->provincia == 0) {
                $empresas = \App\Empresa::select('empresa')->where('empresa', 'LIKE', '%'.$request->search.'%')->take(6)->get();
                return \response()->json($empresas);
            }
            else {
                $provincia = \App\Provincia::find($request->provincia);

                $empresas = $provincia->empresas()->select('empresa')->where('empresa', 'LIKE', '%'.$request->search.'%')->take(6)->get();
                return \response()->json($empresas);
            }
        }
    }

    protected function random_string()
    {
        $key = '';
        $keys = array_merge( range('a','z'), range(0,9) );

        for($i=0; $i<10; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
}
