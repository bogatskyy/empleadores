<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function comentario()
    {
        return $this->belongsTo('App\Comentario');
    }
}
