<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    public function cps()
    {
        return $this->hasMany('App\Cp');
    }

    public function provincia()
    {
        return $this->belongsTo('App\Provincia');
    }
}
