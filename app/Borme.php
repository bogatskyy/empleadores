<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borme extends Model
{

    public function provincias()
    {
        return $this->belongsToMany('App\Provincia');
    }

    public function seccions()
    {
        return $this->belongsToMany('App\Seccion');
    }

    public function empresas()
    {
        return $this->belongsToMany('App\Empresa');
    }
}
