<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    public function bormes()
    {
        return $this->belongsToMany('App\Borme');
    }

    public function localidads()
    {
        return $this->hasMany('App\Localidad');
    }

    public function empresas()
    {
        return $this->belongsToMany('App\Empresa');
    }
}
