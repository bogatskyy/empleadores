<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('salario', 'like');
    }

    public function respuestas()
    {
        return $this->hasMany('App\Respuesta');
    }

    public function usersLikes()
    {
        return $this->belongsToMany('App\User', 'user_like_comentario', 'comentario_id', 'user_id')->withPivot('like', 'dislike');
    }
}
