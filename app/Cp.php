<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cp extends Model
{
    public function localidad()
    {
        return $this->belongsTo('App\Localidad');
    }
}
