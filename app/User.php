<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'provider_name', 'provider_id', 'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function empresas()
    {
        return $this->belongsToMany('App\Empresa')->withPivot('propietario', 'valoracion', 'equilibrio', 'beneficios', 'estabilidad', 'gestion', 'cultura');
    }

    public function comentarios()
    {
        return $this->belongsToMany('App\Comentario')->withPivot('salario', 'like');
    }

    public function respuestas()
    {
        return $this->belongsToMany('App\Respuesta');
    }

    public function empleos()
    {
        return $this->belongsToMany('App\Empleo');
    }

    public function comentariosLikes()
    {
        return $this->belongsToMany('App\Comentario', 'user_like_comentario', 'comentario_id', 'user_id')->withPivot('like', 'dislike');
    }

    public function roles()
    {
        return $this
            ->belongsToMany('App\Role')
            ->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }
    public function propietarioEmpresas () {
        return $this->empresas()
            ->wherePivot('propietario','=', true)
            ->get()
            ->first();
    }
}
