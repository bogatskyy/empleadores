<div class="container">
    <div class="card text-center bg-light">
        <div class="card-header">
            Empresas mas comentadas:
        </div>
        <div class="row">
            @foreach($empresas as $empresa)
            <div class="col-sm-4 mb-3 d-flex">

                <div class="card flex-fill">
                    <div class="card-body">
                        <h6 class="card-title">{{$empresa->empresa}}</h6>
                        <p class="card-text">
                            @foreach(range(1,5) as $i)
                                <span class="fa-stack" style="width:1em">
                    <i class="far fa-star fa-stack-1x"></i>

                                    @if($empresa->total_valoracion >0)
                                        @if($empresa->total_valoracion >0.5)
                                            <i class="fas fa-star fa-stack-1x"></i>
                                        @else
                                            <i class="fas fa-star-half fa-stack-1x"></i>
                                        @endif
                                    @endif
                                    @php $empresa->total_valoracion--; @endphp
                </span>
                            @endforeach
                                valoraciones({{count($empresa->users)}})</p>
                        @if ($empresa->total_comentarios != null)
                        <p class="card-text text-muted">{{$empresa->total_comentarios}} comentarios</p>
                        @endif
                    </div>
                    <div class="card-footer">
                        <a href="{{url('empresas/'.$empresa->empresa)}}" class="btn btn-primary">Mirar información</a>
                        <a href="{{url('/valorar/'.$empresa->empresa)}}" class="btn btn-primary">Valorar</a>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
        <div class="card-footer text-muted align-self-center">
            {{ $empresas->links() }}
        </div>
    </div>
</div>