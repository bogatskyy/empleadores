<div class="container">
    <div class="card mb-3">
        <img src="{{url('images/'.$empresa->foto)}}" class="card-img-top" style="height: 18rem; position: relative;width: 100%;margin-left: 0;margin-right: 0; size: auto" alt="...">
        <div class="card-body">
            <h6 class="card-title">{{$empresa->empresa}}</h6>
            <p class="card-text">
                @foreach(range(1,5) as $i)
                    <span class="fa-stack" style="width:1em">
                    <i class="far fa-star fa-stack-1x"></i>

                        @if($empresa->total_valoracion >0)
                            @if($empresa->total_valoracion >0.5)
                                <i class="fas fa-star fa-stack-1x"></i>
                            @else
                                <i class="fas fa-star-half fa-stack-1x"></i>
                            @endif
                        @endif
                        @php $empresa->total_valoracion--; @endphp
                </span>
                @endforeach
                valoraciones({{count($empresa->users)}})
                    <a href="{{url('/valorar/'.$empresa->empresa)}}" class="float-right btn btn-outline-primary ml-2"> Valorar</a>
            </p>
            <p class="card-text">
                <small class="text-muted">última valoración añadida: </small>
                @if(auth()->user() != null)
                    @if(auth()->user()->hasRole('propietario') && $empresa->id == auth()->user()->propietarioEmpresas()->id)
                        <a href="{{route('empresas.edit', [$empresa->id])}}" class="float-right btn btn-outline-primary ml-2"> Editar</a>
                    @endif
                    @else
                    <a href="{{url('/oferta/'.$empresa->empresa)}}" class="card-link float-right">¿Eres el propietario de la empresa?</a>
                @endif
            </p>
        </div>
    </div>
</div>
<div class="container">
    <nav>
        <div class="nav nav-tabs justify-content-around" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active col text-center" id="nav-resumen-tab" data-toggle="tab" href="#nav-resumen" role="tab" aria-controls="nav-home" aria-selected="true">Resumen</a>
            <a class="nav-item nav-link col text-center" id="nav-actos-tab" data-toggle="tab" href="#nav-actos" role="tab" aria-controls="nav-actos" aria-selected="false">Actos</a>
            <a class="nav-item nav-link col text-center" id="nav-comentarios-tab" data-toggle="tab" href="#nav-comentarios" role="tab" aria-controls="nav-comentarios" aria-selected="false">Comentarios</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-resumen" role="tabpanel" aria-labelledby="nav-resumen-tab">
            @include('empresas.partials.resumen')
        </div>
        <div class="tab-pane fade" id="nav-actos" role="tabpanel" aria-labelledby="nav-actos-tab">
            @include('empresas.partials.actos')
        </div>
        <div class="tab-pane fade" id="nav-comentarios" role="tabpanel" aria-labelledby="nav-comentarios-tab">
            @include('empresas.partials.comentarios')
        </div>
    </div>
</div>