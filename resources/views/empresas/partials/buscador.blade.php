<style>
    .form-control::-webkit-input-placeholder {
        color: white;
    }
</style>
<div class="card bg-transparent justify-content-center justify-content-center mb-3" style="height: 18rem; background-image: url({{asset('images/c.png')}}); background-repeat: no-repeat;position: relative;width: 100%;margin-left: 0;margin-right: 0; background-size: cover;">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <input type="text" id="empresa" class="form-control text-white" placeholder="empresa" aria-label="Empresa" aria-describedby="addon-wrapping">
            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="container">
                    <select class="custom-select form-control text-white" id="provincia">
                        <option style="background: grey" value="0" selected>todas las provincias</option>
                        @foreach($provincias as $provincia)
                            <option style="background: grey" value="{{$provincia->id}}">{{$provincia->provincia}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <button type="submit" id="submit" class="btn btn-outline-light btn-block text-white">Buscar</button>
            </div>
        </div>
    </div>
</div>

<script>

    $("#empresa").easyAutocomplete({

            url: function (search) {
                //if ($("#empresa").val().length > 3) {
                return "{{route('empresas.search')}}?search=" + search + "&provincia=" + $('#provincia').val();
                //}
            },
            getValue: "empresa",
            cssClasses: 'col-sm-12 col-md-3 col-lg-3 col-xl-3 pl-2 pr-2',
            requestDelay: 1000,
            list: {
                onChooseEvent: function () {
                    var empresa = $("#empresa")[0].value;
                    window.location = "{{url('empresas')}}" + "/" + empresa;
                }
            }
        }
    );

        $("#submit").on('click', function () {
            if ($('#empresa').val().length <= 0 && $('#provincia').val() == 0) {
                alert("Elige la empresa o provincia por favor");
            }
            else {
                window.location = "{{route('empresas.provincias.index')}}" + "/" + $('#provincia option[value='+ $('#provincia').val() +']').text() + "/" + $('#provincia').val() + "/" + $('#empresa').val();
            }
        });

</script>