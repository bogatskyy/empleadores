<div class="container">
    <div class="card mb-3">
        <img src="" class="card-img-top" style="height: 18rem" alt="...">
        <div class="card-body">
            <h6 class="card-title">{{$empresa}}</h6>
            <p class="card-text">
        </div>
    </div>
</div>


<div class="container">
    @auth
        <form method="post" action="guardarValoracion" name="val" id="fomularo">
    @else
                <form method="post" id="fomularo">
    @endauth

        @csrf
        <input hidden required id = "empresaName" name="nombre" value="{{$empresa}}">
        <div class="card mb-3">
            <div class="card-body">

                <h5 class="card-title">Valorar a esta empresa</h5>



                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <h6>Valoración general:</h6>
                    </div>
                    <div class="col-md-6">
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                            <i class="far fa-star fa-stack-1x" id="es0{{$i}}"></i>
                        </span>
                        @endforeach
                        <input hidden required id = "es0" name="es0">
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <p>Equilibrio vida privada/Laboral:</p>
                    </div>
                    <div class="col-md-6">
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                            <i class="far fa-star fa-stack-1x" id="es1{{$i}}"></i>
                        </span>
                        @endforeach
                        <input hidden required id = "es1" name="es1">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <p>Salario/Beneficios contractuales:</p>
                    </div>
                    <div class="col-md-6">
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                            <i class="far fa-star fa-stack-1x" id="es2{{$i}}"></i>
                        </span>
                        @endforeach
                        <input hidden required id = "es2" name="es2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <p>Estabilidad laboral/Facilidad de promoción: </p>
                    </div>
                    <div class="col-md-6">
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                            <i class="far fa-star fa-stack-1x" id="es3{{$i}}"></i>
                        </span>
                        @endforeach
                        <input hidden required id = "es3" name="es3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <p>Gestión: </p>
                    </div>
                    <div class="col-md-6">
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                            <i class="far fa-star fa-stack-1x" id="es4{{$i}}"></i>
                        </span>
                        @endforeach
                        <input hidden required id = "es4" name="es4">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <p>Cultura laboral: </p>
                    </div>
                    <div class="col-md-6">
                        @foreach(range(1,5) as $i)
                            <span class="fa-stack" style="width:1em">
                            <i class="far fa-star fa-stack-1x" id="es5{{$i}}"></i>
                        </span>
                        @endforeach
                        <input hidden required  id = "es5" name="es5">
                    </div>
                </div>
                @auth
                    <button class="btn btn-outline-secondary" type="button" onclick="test();">Enviar</button>
                @else
                    <p>Para valorar debe iniciar sesión o registrarse.</p>
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth

            </div>
        </div>
    </form>
</div>

<script>
    var s=document.getElementsByTagName('i');
    for (var i=0;i<s.length;i++)
    {
        s[i].addEventListener('click', function () {
            console.log(this.id);
            var id = this.id;
            var indice = id.charAt(2);
            //Puntuacion
            var num = id.charAt(3);
            document.getElementById("es"+indice).value = num;
            for (var i = 0; i != 5; i++){
                if((i+1) <= num){
                    document.getElementById("es"+indice+(i+1)).className = "fa fa-star fa-stack-1x";
                } else {
                    document.getElementById("es"+indice+(i+1)).className = "far fa-star fa-stack-1x";
                }
            }
        })
    }

    function test(){
        for (var i = 0; i != 6; i++){
            if(document.getElementById("es"+i).value == ""){
                alert("Todas las estrellas deben ser completadas");
                return;
            }
        }
        document.getElementById("fomularo").submit();
    }
</script>
