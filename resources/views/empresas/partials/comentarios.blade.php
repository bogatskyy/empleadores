<comentarios v-bind:empresa="{{$empresa}}"
             @if (isset(Auth::user()->id))
             v-bind:user_id="{{Auth::user()->id}}"
             v-bind:user="{{Auth::user()}}"
             @else
             v-bind:user_id="0"
            @endif>

</comentarios>