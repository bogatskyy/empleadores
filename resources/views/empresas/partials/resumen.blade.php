<div class="row">
    <div class="col">
        <div class="card border-secondary mb-3" style="max-width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">CIF:</h5>
                <p class="card-text">{{$empresa->cif}}</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card border-secondary mb-3" style="max-width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Antigüedad:</h5>
                <p class="card-text">{{substr($empresa->datos_registrales, strrpos($empresa->datos_registrales, '('), strrpos($empresa->datos_registrales, ')'))}}</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card border-secondary mb-3" style="max-width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Datos registrales:</h5>
                <p class="card-text">{{$empresa->datos_registrales}}</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card border-secondary mb-3" style="max-width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Provincia:</h5>
                <p class="card-text">{{$provincia->provincia}}</p>
            </div>
        </div>
    </div>
</div>
<bormes v-bind:empresa="{{$empresa}}"></bormes>