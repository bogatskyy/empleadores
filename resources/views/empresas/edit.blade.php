@extends('layouts/app')

@section('content')

<div class="container">
    <form id="save" method="POST" action="{{ route('empresas.update', [$empresa->id]) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-4 mx-auto">
                <!-- Mensaje de Error llamado por el JS -->
                <div class="alert alert-danger" id="alerta" style="display: none">Su imagen pesa mas de lo permitido!</div>

                <!-- Mensaje de Error llamado por Laravel -->
                @if (session('alert'))
                    <div class="alert alert-danger">Su imagen pesa mas de lo permitido!</div>
            @endif

            <!-- Empresa -->
                <div class="card" style="width: 22rem;">
                    <input name="_method" type="hidden" value="PATCH">
                    <img id="imagen" src="@if($empresa->foto != null) {{asset('/images/'.$empresa->foto)}} @else http://www.agwestdist.com/storage/img/noimg.png @endif" class="card-img-top" alt="...">

                    <div class="button">
                        <input onchange="comprobarPeso(this)" id="img" data-input="false" type="file" data-badge="false" accept="image/x-png,image/gif,image/jpeg" name="foto" style="display:none" />
                        <a href="" id="upload_link">Cambiar Imagen</a>
                    </div>

                    <div class="card-body">

                        <h5 class="card-title">
                            <label>Empresa: </label>
                            <input required value="{{ $empresa->empresa }}" name="empresa" class="form-control">
                        </h5>
                        <label>Datos registrales : </label>
                        <textarea class="form-control" rows="3" name="datos">@if(isset($empresa->datos_registrales)){{ $empresa->datos_registrales }}@endif</textarea>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <label>nº anuncio: </label>
                            <input required value="{{ $empresa->anuncio_id}}" name="anuncio" class="form-control">
                        </li>
                        <li class="list-group-item">
                            <p>
                                Aquí irá información que se podrá cambiar, por falta de tiempo no lo hacemos ahora! Lo que funciona bien es el control de la imagen!
                            </p>
                        </li>
                    </ul>
                    <div class="card-body">

                        {{csrf_field ()}}
                        <a href="{{url('empresas',$empresa->empresa)}}" class="card-link">	&lt;- Volver</a>
                        <a href="javascript:{}" onclick="document.getElementById('save').submit();" class="card-link" style="color: green;" value = "store">Guardar</a>

                    </div>
                </div>
                <!-- Espacio -->
                <figure class="highlight">
                </figure>

            </div>
        </div>
    </form>
</div>

<script>
    $(function(){
        $("#upload_link").on('click', function(e){
            e.preventDefault();
            $("#img:hidden").trigger('click');
        });
    });

    function comprobarPeso(file) {
        var FileSize = file.files[0].size / 1024 / 1024;

        if (FileSize > 2) {
            //Si el tamaño de la imagen es mas grande que 1 MB.
            show();
            $(file).val('');
        }else{
            //Si es mas pequeño se ejecuta la funcion previewFile().
            hide();
            previewFile();
        }
    }

    function previewFile() {
        var preview = document.getElementById('imagen');
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
    }

    //Funcion para mostrar el mensaje de Error.
    function show(){
        var x = document.getElementById("alerta");
        x.style.display = "block";
    }

    //Funcion para esconder el mensaje de Error.
    function hide() {
        var x = document.getElementById("alerta");
        x.style.display = "none";
    }

</script>
@endsection