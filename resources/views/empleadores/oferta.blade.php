<section class="pricing py-5">
    <div class="container">
        <div class="row">
            <!-- Plus Tier -->
            <div class="col-lg-4">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">de prueba</h5>
                        <h6 class="card-price text-center">€0<span class="period">/primer mes</span></h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>perfil personalizable</strong></li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>posibilidad de contestar oficialmente</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Botón de seguimiento</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>publicación de empleos</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>recogida de curriculums</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Control de información sobre la empresa</li>
                        </ul>
                        <a href="{{url('/formulario/'.$empresa)}}" class="btn btn-block btn-primary text-uppercase">Suscribirse</a>
                    </div>
                </div>
            </div>
            <!-- Plus Tier -->
            <div class="col-lg-4">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
                        <h6 class="card-price text-center">€15<span class="period">/mes</span></h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>perfil personalizable</strong></li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>posibilidad de contestar oficialmente</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Botón de seguimiento</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>publicación de empleos</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>recogida de curriculums</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Control de información sobre la empresa</li>
                        </ul>
                        <a href="{{url('/formulario/'.$empresa)}}" class="btn btn-block btn-primary text-uppercase">Suscribirse</a>
                    </div>
                </div>
            </div>
            <!-- Pro Tier -->
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Pro</h5>
                        <h6 class="card-price text-center">€100<span class="period">/año</span></h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>perfil personalizable</strong></li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>posibilidad de contestar oficialmente</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Botón de seguimiento</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>publicación de empleos</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>recogida de curriculums</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Control de información sobre la empresa</li>
                        </ul>
                        <a href="{{url('/formulario/'.$empresa)}}" class="btn btn-block btn-primary text-uppercase">Suscribirse</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
